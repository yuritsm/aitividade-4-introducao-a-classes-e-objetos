package br.ucsal.bes20212.poo.atividade4;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		Veiculo veiculo = new Veiculo();
		Proprietario proprietario = new Proprietario();
		Endereco endereco = new Endereco();
		CadastroVeiculo(scan, veiculo, proprietario, endereco);
	}

	private static void CadastroVeiculo(Scanner scan, Veiculo veiculo, Proprietario proprietario, Endereco endereco) {
		System.out.println("CADASTRO VEICULO\n"
				+ "\n"
				+ "Placa: ");
		veiculo.placa = scan.next();
		System.out.println("Ano de Fabrica��o:");
		veiculo.ano = scan.nextInt();
		System.out.println("Valor: ");
		veiculo.valor = scan.nextDouble();
		System.out.println("Proprietario: ");
		CadastroProprietario(scan, proprietario, endereco);
	}

	private static void CadastroProprietario(Scanner scan, Proprietario proprietario, Endereco endereco) {
		System.out.println("CADASTRO PROPRIETARIO\n"
				+ "\n"
				+ "CPF: ");
		proprietario.cpf = scan.next();
		System.out.println("Nome: ");
		proprietario.nome = scan.next();
		System.out.println("Telefone: ");
		proprietario.telefone = scan.next();
		System.out.println("Endere�o: ");
		CadastroEndereco(scan, endereco);
	}

	private static void CadastroEndereco(Scanner scan, Endereco endereco) {
		System.out.println("CADASTRO ENDERE�O\n"
				+ "\n"
				+ "CEP: ");
		endereco.cep = scan.next();
		System.out.println("Logradouro: ");
		endereco.logradouro = scan.next();
		System.out.println("Numero: ");
		endereco.numero = scan.next();
		System.out.println("Complemento: ");
		endereco.complemento = scan.next();
		System.out.println("Bairro: ");
		endereco.bairro = scan.next();
	}

}
